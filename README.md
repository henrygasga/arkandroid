## Ark PH - An Ecommerce App

### Library Use

* Databinding  - this is for easily handling datas to display in views
* Lottie - this is use for handling animation icons in app
* Parceler - this is use for serializing data for sharing object on another activity
* Material Star Rating - this is used to display the rating status of the product
* Retrofit - this is use for handling the http request in the app

##### Tools

* Android Studio
* Bitbucket
* Postman

##### Implementation

* Using MVP/MVVM as main design architecture
* Uses fragment for handling views.



