package com.ark.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDialog
import com.ark.interfaces.DialogClickInterface
import com.ark.utils.ActivityType
import com.ark.utils.DialogFactory
import com.ark.utils.ObjectSingleton
import com.ark.views.activities.HomeActivity
import com.ark.views.activities.SignUpActivity

open class BaseActivity : AppCompatActivity() {

    var objSingleton: ObjectSingleton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun launchActivity(act: Activity, moveTo: ActivityType) {
        var intent = Intent(act, HomeActivity::class.java)
        when(moveTo) {
            ActivityType.HOME -> {
                // do nothing
            }
            ActivityType.SIGNUP -> {
                intent = Intent(act, SignUpActivity::class.java)
            }
        }
        act.startActivity(intent)
//        finish()
    }

    fun showErrorMsg(msg: String, dialog: AppCompatDialog? = null) =
        DialogFactory.createOkErrorDialog(this, msg, object : DialogClickInterface {
            override fun onPositiveButtonClick() {
                dialog?.dismiss()
            }

            override fun onNegativeButtonClick() {

            }
        }).show()
}