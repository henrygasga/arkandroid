package com.ark.base

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.Fragment
import com.ark.interfaces.DialogClickInterface
import com.ark.utils.DialogFactory

open class BaseFragment : Fragment() {
    protected lateinit var mContext: Context
    protected lateinit var mActivity: Activity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        mActivity = context as Activity
    }

    fun showSnackBar(){

    }

    fun showErrorMsg(msg: String, ctx: Context, dialog: AppCompatDialog? = null) =
        DialogFactory.createOkErrorDialog(ctx, msg, object : DialogClickInterface {
            override fun onPositiveButtonClick() {
                dialog?.dismiss()
            }

            override fun onNegativeButtonClick() {

            }
        }).show()

}