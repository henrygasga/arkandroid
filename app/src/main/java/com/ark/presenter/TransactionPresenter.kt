package com.ark.presenter

import android.content.Context
import android.util.Log
import com.ark.R
import com.ark.api.ApiFactory
import com.ark.interfaces.TransactionInterface
import com.ark.utils.Const
import com.ark.utils.ObjectSingleton
import com.ark.utils.Util
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class TransactionPresenter(context: Context) : TransactionInterface.Presenter, CoroutineScope {

    var ctx: Context = context
    val service = ApiFactory.api
    var viewMyTransaction: TransactionInterface.MyTransaction? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun getMyTransaction() {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val request = service.getMyTransaction(
                     "${Const.GET_TRANSACTIONS}${ObjectSingleton.userLogin?.gen_mid}"
                )
                try {
                    val response = request.await()
                    if (response.body()!!.code == 0) {
                        val msg = response.body()!!.message
                        viewMyTransaction?.onFailedTransaction(msg)
                    } else {
                        Log.d("TransactionPresenter","${response.body()!!.data}")
                        viewMyTransaction?.onSuccessTransaction(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewMyTransaction?.onFailedTransaction(ctx.getString(R.string.error_post_request))
                }
            }
        } else {
            viewMyTransaction?.onFailedTransaction(ctx.getString(R.string.no_internet_connection))
        }
    }
}