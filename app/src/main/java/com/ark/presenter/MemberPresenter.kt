package com.ark.presenter

import android.content.Context
import android.util.Log
import com.ark.R
import com.ark.api.ApiFactory
import com.ark.interfaces.MemberInterface
import com.ark.responses.MemData
import com.ark.utils.Const
import com.ark.utils.Util
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class MemberPresenter(context: Context) : MemberInterface.Presenter, CoroutineScope {


    var ctx: Context = context
    val service = ApiFactory.api
    var postMember: MemberInterface.PostMember? = null
    var viewMyMembers: MemberInterface.MyMembers? = null
    var viewMyMemWithPurchase: MemberInterface.MemWithPur? = null
    var viewMyMembersTwo: MemberInterface.MyMembersTwo? = null
    var viewMyMembersThree: MemberInterface.MyMembersThree? = null
    var viewMyMembersFour: MemberInterface.MyMembersFour? = null
    var viewLogin: MemberInterface.Login? = null

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun postMember(memData: MemData) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val postRequest = service.postmember(
                    Const.POST_MEMBER,
                    memData.fname,
                    memData.lname,
                    memData.email,
                    memData.mobilenum,
                    memData.gender,
                    memData.member_type,
                    memData.password,
                    memData.recruit_code
                )

                try {
                    val response = postRequest.await()

                    if (response.body()!!.code == 0) {
                        val msg = response.body()!!.message
                        postMember?.onPostMemberError(msg)
                    } else {
                        Log.d("MemberPresenter","${response.body()!!.data}")
                        postMember?.onPostMemberSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    postMember?.onPostMemberError(ctx.getString(R.string.error_post_request))
                }
            }
        } else {
            postMember?.onPostMemberError(ctx.getString(R.string.no_internet_connection))
        }
    }

    override fun getMyMember(code: String, i: Int) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val mmembers = service.getMyDownline(
                    Const.MY_MEMBER_CODE + code
                )

                Log.d("onSuccessMyMembers","${Const.MY_MEMBER_CODE + code}")

                Log.d("onSuccessMyMembers","$i")
                try {
                    val response = mmembers.await()

                    if (response.body()!!.code == 0) {
                        val msg = response.body()!!.message
                        viewMyMembers?.onFailed(msg)
                    } else {
                        Log.d("MemberPresenter","${response.body()!!.data}")
                        viewMyMembers?.onSuccessMyMembers(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewMyMembers?.onFailed(ctx.getString(R.string.error_post_request))
                }
            }
        } else {
            viewMyMembers?.onFailed(ctx.getString(R.string.no_internet_connection))
        }
    }

    override fun getMemberWithPurchase(code: String) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val mempur = service.getMemWithPurchase(
                    Const.GET_MEMBER_WITH_PURCHASE + code
                )
                try {
                    val response = mempur.await()
                    Log.d("getMemberWithPurchase","${response.body()!!.data}")
                    if (response.body()!!.code == 0) {
                        val msg = response.body()!!.message
                        viewMyMemWithPurchase?.onFailed(msg)
                    } else {
                        viewMyMemWithPurchase?.onGettingMemWithPurchase(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewMyMemWithPurchase?.onFailed(ctx.getString(R.string.error_post_request))
                }
            }
        } else {
            viewMyMemWithPurchase?.onFailed(ctx.getString(R.string.no_internet_connection))
        }
    }

    override fun login(email: String, pass: String) {
        if(Util.isOnline(ctx)) {
            GlobalScope.launch(Dispatchers.Main) {
                val login = service.loginMember(
                    Const.DO_LOGIN,
                    email,
                    pass
                )

                try {
                    val response = login.await()
                    Log.d("LOGINRESPONSE","$response")
                    Log.d("LOGINRESPONSE","${response.body()!!}")
                    if (response.body()!!.code == 0) {
                        val msg = response.body()!!.message
                        viewLogin?.onFailed(msg)
                    } else {
                        Log.d("MemberPresenter","${response.body()!!.data}")
                        viewLogin?.onLoginSuccess(response.body()!!.data)
                    }
                } catch (e: Exception) {
                    viewLogin?.onFailed(ctx.getString(R.string.error_post_request))
                }
            }
        } else {
            viewLogin?.onFailed(ctx.getString(R.string.no_internet_connection))
        }
    }

    override fun findMember(memData: MemData) {
        if(Util.isOnline(ctx)) {

        } else {
//            offline()
        }
    }

    override fun putMember(memData: MemData) {
        if(Util.isOnline(ctx)) {

        } else {
//            offline()
        }
    }

    override fun deleteMember(memData: MemData) {
        if(Util.isOnline(ctx)) {

        } else {
//            offline()
        }
    }

    fun offline(vh: Any) {
//        vh?.onPostMemberError(ctx.getString(R.string.no_internet_connection))
    }
}