package com.ark.views.fragments

import android.os.Bundle
import android.view.*
import com.ark.R
import com.ark.base.BaseFragment
import com.ark.views.activities.HomeActivity
import kotlinx.android.synthetic.main.toolbar.*

class CategoryFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_category,container,false)
        setHasOptionsMenu(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val parentActivity = (activity as HomeActivity)
        parentActivity.setSupportActionBar(toolbar)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.cart_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}