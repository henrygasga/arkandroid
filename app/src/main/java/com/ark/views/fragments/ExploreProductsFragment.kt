package com.ark.views.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.ark.R
import com.ark.adapter.GenericAdapter
import com.ark.base.BaseFragment
import com.ark.interfaces.ItemClickInterface
import com.ark.models.ProductsData
import com.ark.views.activities.HomeActivity
import com.ark.views.activities.ProductActivity
import com.ark.views.vms.TopSaleViewModel
import kotlinx.android.synthetic.main.fragment_tabs_content.*

class ExploreProductsFragment : BaseFragment() {
    private lateinit var topSaleViewModel: TopSaleViewModel

    private var items: ArrayList<ProductsData> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        topSaleViewModel = ViewModelProviders.of(this).get(TopSaleViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_tabs_content, container, false)
//        val textView: TextView = root.findViewById(R.id.section_label)
//        topSaleViewModel.text.observe(this, Observer<String> {
//            textView.text = it
//        })


        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



//        itemclickprods.setOnClickListener {
//
//        }

        for (i in 1..20){
            items.add(
                ProductsData(
                    "Member Number $i",
                    "8875683535835638"
                )
            )
        }

        rvProducts.apply {
            layoutManager = GridLayoutManager(activity, 2)
            adapter = GenericAdapter(items,R.layout.item_products_topsale,com.ark.BR.model,
                object : ItemClickInterface<ProductsData>{
                    override fun onItemClick(v: View?, item: ProductsData, position: Int) {
                        val intent = Intent(activity, ProductActivity::class.java)
                        (activity as HomeActivity).startActivity(intent)
                    }

                })
        }

    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): ExploreProductsFragment {
            return ExploreProductsFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}