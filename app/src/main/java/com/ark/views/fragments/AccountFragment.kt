package com.ark.views.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialog
import com.ark.R
import com.ark.base.BaseFragment
import com.ark.interfaces.MemberInterface
import com.ark.presenter.MemberPresenter
import com.ark.responses.MemData
import com.ark.utils.ActivityType
import com.ark.utils.DialogFactory
import com.ark.utils.ObjectSingleton
import com.ark.utils.ScreenType
import com.ark.views.activities.HomeActivity
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.fragment_account.*
import org.json.JSONObject
import java.util.*

class AccountFragment : BaseFragment(), MemberInterface.Login {

    var memberPresenter: MemberPresenter? = null
    var dialogLoading: AppCompatDialog? = null
    private var callbackManager: CallbackManager? = null
    private var facebookJSONObject: String? = null
    private var isFromLogin = false

    override fun onFailed(msg: String) {
        dialogLoading!!.dismiss()
        showErrorMsg(msg, mContext)
    }

    override fun onLoginSuccess(listMem: List<MemData>) {
        ObjectSingleton.userLogin = listMem[0]
        dialogLoading!!.dismiss()
        val actRunning = activity as HomeActivity

        actRunning.changeContent(ScreenType.PROFILE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val actRunning = activity as HomeActivity

        memberPresenter = MemberPresenter(actRunning)
        memberPresenter?.viewLogin = this
        dialogLoading = DialogFactory.createProgressDialog(actRunning)

        callbackManager = CallbackManager.Factory.create()

        mbSignUp.setOnClickListener {

            actRunning.launchActivity(actRunning, ActivityType.SIGNUP)
        }

        btnLogin.setOnClickListener {
            validateRequest()
        }

        fbLogin.setOnClickListener {
//            performFaceBookLogin()
        }
    }

    private fun callFacebookLogin(fbId: String) {
        val jsonObject = JSONObject()
        jsonObject.put("device_id", "android")
//        jsonObject.put(
//            "device_token",
//            "", "")
//        )
        jsonObject.put("device_type", 1)
        jsonObject.put("fb_id", fbId)

        Log.d("FACEBOOKLOGIN","result fb = $jsonObject")
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }


    fun performFaceBookLogin() {
        LoginManager.getInstance()
            .logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))

        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                Log.d("FACEBOOKLOGIN","FTOKEN==" + result?.accessToken?.token)
                val request =
                    GraphRequest.newMeRequest(result?.accessToken, object : GraphRequest.GraphJSONObjectCallback {
                        override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                            facebookJSONObject = `object`.toString()

                            callFacebookLogin(`object`?.getString("id").toString())
                        }
                    })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {

            }

            override fun onError(error: FacebookException?) {
                error?.printStackTrace()
            }

        })
    }

    fun validateRequest() {
        if(mtv_email.text.toString().isEmpty() ||
            mtv_password.text.toString().isEmpty()) {
            showErrorMsg("All Fields required.", mContext)
        } else {
            dialogLoading!!.show()
            memberPresenter?.login(
                mtv_email.text.toString(),
                mtv_password.text.toString()
            )
        }
    }
}