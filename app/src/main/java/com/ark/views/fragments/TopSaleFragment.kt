package com.ark.views.fragments

import android.app.Activity
import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.ark.R
import com.ark.adapter.SectionsPagerAdapter
import com.ark.base.BaseFragment
import com.ark.views.activities.HomeActivity
import com.ark.views.vms.TopSaleViewModel
import kotlinx.android.synthetic.main.fragment_topsale.*
import kotlinx.android.synthetic.main.toolbar.*

class TopSaleFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_topsale,container,false)
        setHasOptionsMenu(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val parentActivity = (activity as HomeActivity)
        parentActivity.setSupportActionBar(toolbar)

        val sectionsPagerAdapter = SectionsPagerAdapter(parentActivity, childFragmentManager)
        view_pager.apply {
            adapter = sectionsPagerAdapter
        }
        tabs.apply {
            setupWithViewPager(view_pager)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.cart_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}