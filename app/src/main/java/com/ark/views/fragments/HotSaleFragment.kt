package com.ark.views.fragments

import android.os.Bundle
import android.view.*
import com.ark.R
import com.ark.base.BaseFragment
import com.ark.views.activities.HomeActivity
import kotlinx.android.synthetic.main.toolbar.*

class HotSaleFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_hotsale,container,false)
        setHasOptionsMenu(true)

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as HomeActivity).setSupportActionBar(toolbar)
//        toolbar.title = "Pogi ni Henry"
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.cart_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}