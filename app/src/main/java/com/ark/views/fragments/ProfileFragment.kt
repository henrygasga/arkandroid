package com.ark.views.fragments

import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ark.R
import com.ark.adapter.GenericAdapter
import com.ark.base.BaseFragment
import com.ark.interfaces.ItemClickInterface
import com.ark.interfaces.MemberInterface
import com.ark.interfaces.TransactionInterface
import com.ark.models.MemberData
import com.ark.presenter.MemberPresenter
import com.ark.presenter.TransactionPresenter
import com.ark.responses.MemData
import com.ark.responses.MemberWithPurchaseData
import com.ark.responses.TransactionData
import com.ark.utils.DialogFactory
import com.ark.utils.ObjectSingleton
import com.ark.utils.ScreenType
import com.ark.utils.Util
import com.ark.views.activities.HomeActivity
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment(), MemberInterface.MyMembers, MemberInterface.MemWithPur, TransactionInterface.MyTransaction {

    override fun onFailedTransaction(msg: String) {
        dialogLoading!!.dismiss()
    }

    override fun onSuccessTransaction(data: List<TransactionData>) {

    }

    override fun onFailed(msg: String) {
        dialogLoading!!.dismiss()
//        showErrorMsg(msg, mContext)
    }

    // Robert - AKAFP619
    // bryan - A9BW1619

    override fun onSuccessMyMembers(listMem: List<MemData>) {

    }

    private var items: List<MemData> = arrayListOf()
    private var newitems: List<MemData> = arrayListOf()
    var memberPresenter: MemberPresenter? = null
    var tranactionPresenter: TransactionPresenter? = null
    var dialogLoading: AppCompatDialog? = null

    override fun onResume() {
        super.onResume()
        if(ObjectSingleton.userLogin == null) {
            (mActivity as HomeActivity).changeContent(ScreenType.HOT_SALE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mtv_fullname.text =  "${ObjectSingleton.userLogin?.fname} ${ObjectSingleton.userLogin?.lname}"
        mtv_code.text = "Code: ${ObjectSingleton.userLogin?.gen_mid}"

        mtv_nomembers.visibility = View.VISIBLE
        memberPresenter = MemberPresenter(mActivity)
        tranactionPresenter = TransactionPresenter(mActivity)
        memberPresenter?.viewMyMembers = this
        memberPresenter?.viewMyMemWithPurchase = this
        tranactionPresenter?.viewMyTransaction = this
        dialogLoading = DialogFactory.createProgressDialog(mActivity)
        dialogLoading!!.show()

        memberPresenter?.getMemberWithPurchase(
            ObjectSingleton.userLogin?.gen_mid as String
        )
    }

    override fun onGettingMemWithPurchase(listMem: List<MemberWithPurchaseData>) {
        if(listMem.isNotEmpty())
            mtv_nomembers.visibility = View.GONE

        dialogLoading!!.dismiss()

        for (i in 0 until listMem.size) {
            listMem[i].comm = "P ${Util.formatCurrency((listMem[i].totalpurchase * Util.getPercentage(1)))}"
        }

        rvMembers.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = GenericAdapter(listMem,R.layout.item_members, com.ark.BR.model,
                object : ItemClickInterface<MemberWithPurchaseData>{
                    override fun onItemClick(v: View?, item: MemberWithPurchaseData, position: Int) {
                        ObjectSingleton.recruitCode = item
                        ObjectSingleton.countMemberDisplay = 1
                        (activity as HomeActivity).goToMyMembers()
                    }
                })
            addItemDecoration(DividerItemDecoration(activity, ClipDrawable.HORIZONTAL))
        }
    }
}