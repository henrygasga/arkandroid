package com.ark.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ark.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
