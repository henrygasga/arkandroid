package com.ark.views.activities

import android.content.Intent
import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ark.BR
import com.ark.R
import com.ark.adapter.GenericAdapter
import com.ark.base.BaseActivity
import com.ark.interfaces.ItemClickInterface
import com.ark.interfaces.MemberInterface
import com.ark.presenter.MemberPresenter
import com.ark.responses.MemData
import com.ark.responses.MemberWithPurchaseData
import com.ark.utils.DialogFactory
import com.ark.utils.ObjectSingleton
import com.ark.utils.Util
import kotlinx.android.synthetic.main.activity_mymembers.*
import kotlinx.android.synthetic.main.toolbar.*

class MyMembersActivity : BaseActivity(), MemberInterface.MyMembers, MemberInterface.MemWithPur {

    override fun onFailed(msg: String) {
        dialogLoading!!.dismiss()
//        showErrorMsg(msg)
    }

    override fun onSuccessMyMembers(listMem: List<MemData>) {

    }

    var dialogLoading: AppCompatDialog? = null
    private var items: List<MemData> = arrayListOf()
    var memberPresenter: MemberPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mymembers)

        setSupportActionBar(toolbar)
        toolbar.title = getString(R.string.app_name)

        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)

        memberPresenter = MemberPresenter(this)
        memberPresenter?.viewMyMembers = this
        memberPresenter?.viewMyMemWithPurchase = this
        dialogLoading = DialogFactory.createProgressDialog(this)
        dialogLoading!!.show()

        tvRecruit.text = "${ObjectSingleton.recruitCode?.fname}'s Recruit"
        mtv_nomembers.visibility = View.VISIBLE

        memberPresenter?.getMemberWithPurchase(
            ObjectSingleton.recruitCode?.gen_mid as String
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onGettingMemWithPurchase(listMem: List<MemberWithPurchaseData>) {
        if(listMem.isNotEmpty())
            mtv_nomembers.visibility = View.GONE

        dialogLoading!!.dismiss()


        if(ObjectSingleton.countMemberDisplay == 1) {
            ObjectSingleton.countMemberDisplay++
        }

        for (i in 0 until listMem.size) {
            listMem[i].comm = "P ${Util.formatCurrency((listMem[i].totalpurchase * Util.getPercentage(ObjectSingleton.countMemberDisplay)))}"
        }
        rvList.apply {
            layoutManager = LinearLayoutManager(this@MyMembersActivity)
            adapter = GenericAdapter(listMem,R.layout.item_members, BR.model,
                object : ItemClickInterface<MemberWithPurchaseData> {
                    override fun onItemClick(v: View?, item: MemberWithPurchaseData, position: Int) {
                        ObjectSingleton.recruitCode = item
                        ObjectSingleton.countMemberDisplay++
                        Log.d("ObjectSingleton","${ObjectSingleton.countMemberDisplay}")
                        if(ObjectSingleton.countMemberDisplay < 4) {
                            val intent = Intent(this@MyMembersActivity, MyMembersActivity::class.java)
                            startActivity(intent)
                        }
                    }
                })
            addItemDecoration(DividerItemDecoration(this@MyMembersActivity, ClipDrawable.HORIZONTAL))
        }
    }
}