package com.ark.views.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ark.R
import com.ark.base.BaseActivity
import com.ark.utils.DialogFactory
import com.ark.utils.ObjectSingleton
import com.ark.utils.ScreenType
import com.ark.views.fragments.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_hotsale -> {
                changeContent(ScreenType.HOT_SALE)
                return true
            }
            R.id.nav_topsale -> {
                changeContent(ScreenType.TOP_SALE)
                return true
            }
            R.id.nav_category -> {
                changeContent(ScreenType.CATEGORY)
                return true
            }
            R.id.nav_account -> {
                changeContent(ScreenType.ACCOUNT)
                return true
            }
        }
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        changeContent(ScreenType.HOT_SALE)
        nav_view.setOnNavigationItemSelectedListener(this)

        DialogFactory.createAddsDialog(this).show()
    }

    fun goToMyMembers() {
        val intent = Intent(this, MyMembersActivity::class.java)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.nav_logout -> {
                ObjectSingleton.userLogin = null
                Toast.makeText(this,"Logout",Toast.LENGTH_LONG).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun changeContent(screenType: ScreenType) {
        var fragment: Fragment = HotSaleFragment()

        when(screenType) {
            ScreenType.HOT_SALE -> {
                // do nothing
            }
            ScreenType.TOP_SALE -> {
                fragment = TopSaleFragment()
            }
            ScreenType.CATEGORY -> {
                fragment = CategoryFragment()
            }
            ScreenType.ACCOUNT -> {
                if(ObjectSingleton.userLogin != null) {
                    fragment = ProfileFragment()
                } else {
                    fragment = AccountFragment()
                }
            }
            ScreenType.PROFILE -> {
                fragment = ProfileFragment()
            }
        }

        supportFragmentManager.beginTransaction()
            .replace(R.id.frame_layout_content, fragment)
            .addToBackStack(null)
            .commit()
    }
}