package com.ark.views.activities

import android.content.Intent
import android.os.Bundle
import com.ark.R
import com.ark.base.BaseActivity
import com.ark.utils.ActivityType
import com.ark.utils.Const
import com.ark.utils.ObjectSingleton
import com.ark.views.MainActivity

class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        ObjectSingleton.baseUrl = Const.HOST
            .plus(getString(R.string.base_host))
            .plus(Const.PORT)
            .plus(Const.ROOT)
    }

    fun moveToLogin() {
//        val intent = Intent(this, IntroActivity::class.java)
//        startActivity(intent)
        launchActivity(this, ActivityType.HOME)
        finish()
    }
}