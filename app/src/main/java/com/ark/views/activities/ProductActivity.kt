package com.ark.views.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import com.ark.R
import com.ark.base.BaseActivity
import com.ark.utils.ScreenType
import kotlinx.android.synthetic.main.toolbar.*

class ProductActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        setSupportActionBar(toolbar)

        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)

//        changeContent(ScreenType.HOT_SALE)
//        nav_view.setOnNavigationItemSelectedListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.cart_menu, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}