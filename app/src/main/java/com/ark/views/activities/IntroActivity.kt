package com.ark.views.activities

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import com.ark.R
import com.ark.base.BaseActivity
import com.ark.utils.ActivityType
import kotlinx.android.synthetic.main.activity_intro.*

class IntroActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        start_shopping.setOnClickListener {
            launchActivity(this, ActivityType.HOME)
        }

    }
}