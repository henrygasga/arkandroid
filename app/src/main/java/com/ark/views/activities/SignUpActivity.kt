package com.ark.views.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDialog
import com.ark.R
import com.ark.base.BaseActivity
import com.ark.interfaces.DialogClickInterface
import com.ark.interfaces.MemberInterface
import com.ark.presenter.MemberPresenter
import com.ark.responses.MemData
import com.ark.utils.DialogFactory
import com.ark.utils.ObjectSingleton
import kotlinx.android.synthetic.main.activity_create_account.*

class SignUpActivity : BaseActivity(), MemberInterface.PostMember {

    var memberPresenter: MemberPresenter? = null
    var dialogLoading: AppCompatDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)

        memberPresenter = MemberPresenter(this)
        memberPresenter?.postMember = this
        dialogLoading = DialogFactory.createProgressDialog(this)


        btn_signup.setOnClickListener {
            validateRequest()
        }
    }

    fun validateRequest() {
        if(mtv_fname.text.toString().isEmpty() ||
                mtv_lname.text.toString().isEmpty() ||
                mtv_code.text.toString().isEmpty() ||
                mtv_email.text.toString().isEmpty() ||
                mtv_password.text.toString().isEmpty()) {
            showErrorMsg("All Fields required.")
        } else {
            dialogLoading!!.show()
            memberPresenter?.postMember(
                MemData(
                    0,"","",
                    mtv_email.text.toString(),
                    mtv_fname.text.toString(),
                    mtv_mobile.text.toString(),
                    "","","",
                    mtv_lname.text.toString(),
                    "Gold",
                    mtv_password.text.toString(),
                    mtv_code.text.toString().toUpperCase()
                )
            )
        }
    }

    fun moveTo() {
        val intent = Intent(this, IntroActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onPostMemberSuccess(listMem: List<MemData>) {
        ObjectSingleton.userLogin = listMem[0]
        dialogLoading!!.dismiss()

        val that = this
        DialogFactory.createOkErrorDialog(this, "Registered Successful!", object : DialogClickInterface {
            override fun onPositiveButtonClick() {
                val intent = Intent(that, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun onNegativeButtonClick() {

            }
        }).show()

    }

    override fun onPostMemberError(msg: String) {
        dialogLoading!!.dismiss()
        showErrorMsg(msg)
    }
}