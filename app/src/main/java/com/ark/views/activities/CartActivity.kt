package com.ark.views.activities

import android.os.Bundle
import com.ark.R
import com.ark.base.BaseActivity

class CartActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
    }
}