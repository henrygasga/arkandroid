package com.ark.models

import android.os.Parcel
import android.os.Parcelable

data class MemberData(
    val memName: String?,
    val memDetails: String?
)