package com.ark.models

import android.os.Parcel
import android.os.Parcelable

data class ProductsData(
    val prodId: String?,
    val prodTitle: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(prodId)
        parcel.writeString(prodTitle)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductsData> {
        override fun createFromParcel(parcel: Parcel): ProductsData {
            return ProductsData(parcel)
        }

        override fun newArray(size: Int): Array<ProductsData?> {
            return arrayOfNulls(size)
        }
    }
}