package com.ark.api

import com.ark.responses.MemberWithPurchase
import com.ark.responses.PostMember
import com.ark.responses.PostTransaction
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST
    fun postmember(
        @Url urlEndpoint: String,
        @Field("fname") fname: String,
        @Field("lname") lname: String,
        @Field("email") email: String,
        @Field("mobilenum") mobilenum: String,
        @Field("gender") gender: String,
        @Field("membertype") membertype: String,
        @Field("password") password: String,
        @Field("recruit_code") recruit_code: String
    ) : Deferred<Response<PostMember>>

    @GET
    fun getMyDownline(
        @Url urlEndpoint: String
    ) : Deferred<Response<PostMember>>

    @FormUrlEncoded
    @POST
    fun loginMember(
        @Url urlEndpoint: String,
        @Field("email") email: String,
        @Field("password") password: String
    ) : Deferred<Response<PostMember>>

    @GET
    fun getMyTransaction(
        @Url urlEndpoint: String
    ) : Deferred<Response<PostTransaction>>

    @FormUrlEncoded
    @POST
    fun processTransaction(
        @Url urlEndpoint: String,
        @Field("member_id") member_id: String,
        @Field("totalpurchase") totalpurchase: String,
        @Field("transtatus") transtatus: String
    ) : Deferred<Response<PostTransaction>>

    @GET
    fun getMemWithPurchase(
        @Url urlEndpoint: String
    ) : Deferred<Response<MemberWithPurchase>>
}