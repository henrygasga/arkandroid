package com.ark.responses

data class MemberWithPurchase(
    val code: Int,
    val `data`: List<MemberWithPurchaseData>,
    val message: String
)

data class MemberWithPurchaseData(
    val _id: String,
    val create_date: String,
    val email: String,
    val fname: String,
    val gen_mid: String,
    val gender: String,
    val isDeleted: String,
    val lname: String,
    val member_type: String,
    val password: String,
    val recruit_code: String,
    var totalpurchase: Int,
    var comm: String
)