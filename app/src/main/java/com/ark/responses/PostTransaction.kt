package com.ark.responses

data class PostTransaction(
    val code: Int,
    val `data`: List<TransactionData>,
    val message: String
)

data class TransactionData(
    val __v: Int,
    val _id: String,
    val create_date: String,
    val memid: String,
    val referencenum: String,
    val totalpurchase: String,
    val transtatus: String
)