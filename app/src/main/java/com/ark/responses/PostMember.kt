package com.ark.responses

data class PostMember(
    val code: Int,
    val `data`: List<MemData>,
    val message: String
)

data class MemData(
    val __v: Int,
    val _id: String,
    val create_date: String,
    val email: String,
    val fname: String,
    val mobilenum: String,
    val gen_mid: String,
    val gender: String,
    val isDeleted: String,
    val lname: String,
    val member_type: String,
    val password: String,
    val recruit_code: String,
    val commision: String? = null
)