package com.ark.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.ark.views.fragments.ExploreProductsFragment

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return ExploreProductsFragment.newInstance(position + 1)
    }

    override fun getCount(): Int {
        return 5
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return TAB_TITLES[position]
    }

    private val TAB_TITLES = arrayOf(
        "All",
        "Shoes",
        "Dress",
        "Laptop",
        "Tools"
    )


}