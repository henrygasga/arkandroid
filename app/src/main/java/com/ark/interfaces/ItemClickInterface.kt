package com.ark.interfaces

import android.view.View

interface ItemClickInterface<T> {
    fun onItemClick(v:View?, item: T, position: Int)
}