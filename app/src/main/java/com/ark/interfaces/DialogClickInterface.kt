package com.ark.interfaces

interface DialogClickInterface {
    fun onPositiveButtonClick()
    fun onNegativeButtonClick()
}