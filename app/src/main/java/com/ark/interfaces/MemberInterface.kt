package com.ark.interfaces

import com.ark.responses.MemData
import com.ark.responses.MemberWithPurchaseData

interface MemberInterface {
    interface Base {
        fun onFailed(msg: String)
    }
    interface PostMember {
        fun onPostMemberSuccess(listMem: List<MemData>)
        fun onPostMemberError(msg: String)
    }
    interface MyMembers : Base {
        fun onSuccessMyMembers(listMem: List<MemData>)
    }
    interface MyMembersTwo : Base {
        fun onSuccessMyMembers(listMem: List<MemData>)
    }
    interface MyMembersThree : Base {
        fun onSuccessMyMembers(listMem: List<MemData>)
    }
    interface MyMembersFour : Base {
        fun onSuccessMyMembers(listMem: List<MemData>)
    }
    interface Login : Base {
        fun onLoginSuccess(listMem: List<MemData>)
    }
    interface MemWithPur : Base {
        fun onGettingMemWithPurchase(listMem: List<MemberWithPurchaseData>)
    }
    interface Presenter {
        fun postMember(memData: MemData)
        fun login(email: String, pass: String)
        fun getMyMember(code: String, i: Int)
        fun getMemberWithPurchase(code: String)
        fun findMember(memData: MemData)
        fun putMember(memData: MemData)
        fun deleteMember(memData: MemData)
    }
}