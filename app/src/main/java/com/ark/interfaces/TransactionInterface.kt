package com.ark.interfaces

import com.ark.responses.TransactionData

interface TransactionInterface {
    interface Base {
        fun onFailedTransaction(msg: String)
    }
    interface MyTransaction : Base {
        fun onSuccessTransaction(data: List<TransactionData>)
    }
    interface Presenter {
        fun getMyTransaction()
    }
}