package com.ark.utils

class Const {
    companion object {
        var HOST: String = "http://"
        var PORT: String = ":8001"
        var ROOT: String = "/api/"
//        var BASE_URL: String = host.plus()

        //API ENDPOINTS
        var POST_MEMBER: String = "members"
        var MY_MEMBER_CODE: String = "members/code/"
        var DO_LOGIN: String = "login"
        var GET_TRANSACTIONS: String = "transaction/"
        var GET_MEMBER_WITH_PURCHASE: String = "members/codewithcom/"
    }
}