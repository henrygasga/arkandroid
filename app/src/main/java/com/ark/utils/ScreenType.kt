package com.ark.utils

enum class ScreenType {
    HOT_SALE,
    TOP_SALE,
    CATEGORY,
    ACCOUNT,
    PROFILE
}