package com.ark.utils

import android.content.Context
import android.net.ConnectivityManager
import java.text.DecimalFormat

class Util {
    companion object {
        @JvmStatic
        fun isOnline(cContext: Context): Boolean {
            try {
                val cm = cContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo = cm.activeNetworkInfo
                if (netInfo != null && netInfo.isConnectedOrConnecting) {
                    return true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return false
        }

        @JvmStatic
        fun getPercentage(count: Int): Double {
            return when(count) {
                1 -> 0.8
                2 -> 0.7
                3 -> 0.6
                else -> 0.2
            }
        }

        @JvmStatic
        fun formatCurrency(amount: Double): String {
            val formatter = DecimalFormat("#,##0.00")
            return formatter.format(amount)
        }
    }
}