package com.ark.utils

import com.ark.responses.MemData
import com.ark.responses.MemberWithPurchaseData

object ObjectSingleton {
    var baseUrl: String? = null
    var userLogin: MemData? = null
    var recruitCode: MemberWithPurchaseData? = null
    var countMemberDisplay: Int = 0
    var oneComm: Double? = 0.0
    var secLoad = false
    var thirdLoad = false
}